# CSC 1103 – Programming Methodology Mini-project 2023

This assignment seeks develop the 3x3 Tic Tac Toe game in C programming language with the following features:

1. two player mode where two kids can play with each other 
2. A one-player mode where the sole kid will play with AI enabled computer 

The design of the IoT game should come with user-friendly graphical user interface (GUI)

## Installation


## Authors and acknowledgment

CSC1104 P3 Grp 6 T1 AY2023/24

## License

©2023 https://gitlab.com/2301020

## Project status

Currently under development