#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Program States
#define SINGLEPLAYER 1
#define MULTIPLAYER 2
#define EXIT 3

/* FUNCTION PROTOTYPES */
int getGameMode();
void gameMode();
void singlePlayer();
void multiPlayer();
char gridChar();
int win();
void draw();
int minimax();
void computerMove();
void playerMove();
int getDifficulty();

/* MAIN PROGRAM */
int main() {
    gameMode();
}

/* Selection of gamemode */
void gameMode(){
    int selection;

    do {
        selection = getGameMode();

        switch(selection) {

            case SINGLEPLAYER:
                singlePlayer();
                break;
            case MULTIPLAYER:
                multiPlayer();
                break;
            case EXIT:
                break;
            default:
                printf("\nInvalid Input. Try Again...\n");
                break;
        }

    } while (selection != EXIT);
    
    return;
}

/* The main menu that determines what the user wants to do */
int getGameMode() {

    int selection;

    printf("\n===== TIC TAC TOE =====\n");
    printf("[1] Singleplayer\n");
    printf("[2] Multiplayer\n");
    printf("[3] Exit\n");
    printf("\nYour Selection: ");

    scanf("%d", &selection);

    return selection;
}

/* The main menu that determines what the user wants to do */
int getDifficulty() {

    int difficulty = 0;

    // do{
        
    // }while (difficulty <1 || difficulty >3);

    printf("\n===== Select Difficulty =====\n");
    printf("[1] Easy\n");
    printf("[2] Medium\n");
    printf("[3] Hard\n");
    printf("\nYour Selection: ");
    scanf("%d", &difficulty);

    if(difficulty <1 || difficulty >3){
        // system("cls");
        printf("Invalid Input. Try Again...\n\n");
        getDifficulty();
    }

    return difficulty;
}

void singlePlayer(){

    // system("cls");
    int board[9] = {0,0,0,0,0,0,0,0,0};
    //computer squares are 1, player squares are -1.

    int diff = getDifficulty(); 
    printf("\nDifficulty Selected: %d", diff);

    printf("\nComputer: O, You: X\nPlay (1)st or (2)nd? ");
    int player=0;
    scanf("%d",&player);
    printf("\n");
    unsigned turn;
    for(turn = 0; turn < 9 && win(board) == 0; ++turn) {
        if((turn+player) % 2 == 0)
            computerMove(board);
        else {
            draw(board);
            playerMove(SINGLEPLAYER, board);
        }
    }
    switch(win(board)) {
        case 0:
            draw(board);
            printf("Draw!\n");
            break;
        case 1:
            draw(board);
            printf("You lose.\n");
            break;
        case -1:
            draw(board);
            printf("You win. Outstanding!\n");
            break;
    }
}

void multiPlayer(){
    // system("cls");
    int board[9] = {0,0,0,0,0,0,0,0,0};
    //computer squares are 1, player squares are -1.
    printf("\nPlayer 1: O, Player 2: X");
    int player=0;
    printf("\n");
    unsigned turn;
    for(turn = 0; turn < 9 && win(board) == 0; ++turn) {
        if((turn+player) % 2 == 0){
            draw(board);
            playerMove(MULTIPLAYER, board);
        } else {
            draw(board);
            playerMove(SINGLEPLAYER, board);
        }
    }
    switch(win(board)) {
        case 0:
            draw(board);
            printf("Draw!\n");
            break;
        case 1:
            draw(board);
            printf("Player 1 wins. Outstanding!\n");
            break;
        case -1:
            draw(board);
            printf("Player 2 wins. Inconceivable!\n");
            break;
    }
}

char gridChar(int i) {
    switch(i) {
        case -1:
            return 'X';
        case 0:
            return ' ';
        case 1:
            return 'O';
    }
}

void draw(int b[9]) {
    printf(" %c | %c | %c\n",gridChar(b[0]),gridChar(b[1]),gridChar(b[2]));
    printf("---+---+---\n");
    printf(" %c | %c | %c\n",gridChar(b[3]),gridChar(b[4]),gridChar(b[5]));
    printf("---+---+---\n");
    printf(" %c | %c | %c\n",gridChar(b[6]),gridChar(b[7]),gridChar(b[8]));
}

int win(const int board[9]) {
    //determines if a player has won, returns 0 otherwise.
    unsigned wins[8][3] = {{0,1,2},{3,4,5},{6,7,8},{0,3,6},{1,4,7},{2,5,8},{0,4,8},{2,4,6}};
    int i;
    for(i = 0; i < 8; ++i) {
        if(board[wins[i][0]] != 0 &&
           board[wins[i][0]] == board[wins[i][1]] &&
           board[wins[i][0]] == board[wins[i][2]])
            return board[wins[i][2]];
    }
    return 0;
}

int minimax(int board[9], int player, int alpha, int beta) {

    int winner = win(board);
    if(winner != 0) return winner;
    int move = -1;
    if (player == 1){
        int score = -1000;//Losing moves are preferred to no move
        for(int i = 0; i < 9; ++i) {//For all moves,
            if(board[i] == 0) {//If legal,
                board[i] = player;//Try the move
                int thisScore = minimax(board, -player, alpha, beta);
                board[i] = 0;//Reset board after try
                if(thisScore > score) {
                    score = thisScore;
                    move = i;
                }//Pick the one that's worst for the opponent
                alpha = score;
                if (alpha >= beta) break;
            }
        }
        if(move == -1) return 0;
        return score;
    } else {
        int score = 1000;//Losing moves are preferred to no move
        for(int i = 0; i < 9; ++i) {//For all moves,
            if(board[i] == 0) {//If legal,
                board[i] = player;//Try the move
                int thisScore = minimax(board, -player, alpha, beta);
                board[i] = 0;//Reset board after try
                if(thisScore < score) {
                    score = thisScore;
                    move = i;
                }//Pick the one that's worst for the opponent
                beta = score;
                if (alpha >= beta) break;  // Prune the branch
            }
        }
        if(move == -1) return 0;
        return score;
    }
}

void computerMove(int board[9]) {
    int move = -1;
    int score = -2;
    int i;
    for(i = 0; i < 9; ++i) {
        if(board[i] == 0) {
            board[i] = 1;
            int tempScore = minimax(board, -1, -1000, 1000);
            board[i] = 0;
            if(tempScore > score) {
                score = tempScore;
                move = i;
            }
        }
    }
    //returns a score based on minimax tree at a given node.
    board[move] = 1;
}

void playerMove(int player, int board[9]) {
int move = 0;
do {
    start:
    printf("\nInput move ([0..8]): ");
    scanf("%d", &move);
    if(board[move] != 0) {
        printf("Its Already Occupied !");
        goto start;
    }
    printf("\n");
} while (move >= 9 || move < 0 || board[move] != 0);
    if(player == 1){
        board[move] = -1;
    }
    else if (player == 2)
    {
        board[move] = 1;
    }              
}